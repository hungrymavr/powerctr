/*
 * battery.h
 *
 * Created: 24.09.2014 16:13:15
 *  Author: ale
 */ 


#ifndef BATTERY_H_
#define BATTERY_H_

#define ALARM_VOLTAGE		11000
//#define ALARM_VOLTAGE		15000

typedef struct __attribute__((packed)) {
	uint8_t reserved; // 0x00
	uint8_t status;
	uint8_t unusefull[6]; // 0x02 - 0x07
	uint16_t iavg;
	uint16_t temperature;
	uint16_t voltage;
	uint16_t current;
	uint16_t acr;
	uint16_t acrl;
	uint8_t as;
	uint8_t sfr;
	uint16_t ful;
	uint16_t ae;
	uint16_t se;
	uint8_t reserved_blk0[3]; // 0x1c - 0x1e
	uint8_t eeprom;
	uint8_t unusefull1[224];
} ds2782_mmap_t;

typedef struct {
/* inside state */
	ds2782_mmap_t map;
	bool charging;
	bool connect;
	
	bool (*query)(void);
	bool (*charge)(void);
	bool (*nocharge)(void);	
	bool (*charge_possible)(void);
	bool (*condition)(void);
	
	uint16_t (*get_temp)(void);
	uint32_t (*get_volt)(void);
	uint16_t (*get_current)(void);
	
	register8_t * (*get_addr_map)(register8_t addr);
} battery_t;

battery_t battery;

void battery_init(void);
bool battery_query(void);
bool battery_charge(void);
bool battery_nocharge(void);
bool battery_is_charge_possible(void);
bool battery_is_condition(void);

uint16_t battery_get_temperature(void);
uint32_t battery_get_voltage(void);
uint16_t battery_get_current(void);

register8_t * battery_get_map_address(register8_t addr);



#endif /* BATTERY_H_ */