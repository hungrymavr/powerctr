/*
 * console.c
 *
 * Created: 28.08.2014 16:38:29
 *  Author: ale
 */ 

#include "console.h"
#include "i2c.h"
#include "battery.h"
#include "utils/mcp.h"
#include <string.h>
#include <stdio.h>

void console_init() {

}

void console_send() {
	static unsigned long counter = 0;
	char line[64];
	print( "\033c\t\tPower controller console.");
	sprintf(line, "\r\n\r\nBattery information:");
	print(line);
	sprintf(line, "\r\nVoltage: %lumV; Current: %dmA; Temp: %uC;", battery.get_volt(), battery.get_current(), battery.get_temp());
	print(line);
	
	sprintf(line, "\r\n\r\nDevice information:");
	print(line);
	sprintf(line, "\r\nMCP enable: %s; \r\nCharging: %s;\r\nBort: %s;", 
		is_mcp_enable() ? "yes" : "no", battery.charging ? "yes" : "no", 
		 is_bort() ? "yes" : "no");
	print(line);
	
	sprintf(line, "\r\nBattery: %s;", battery.connect ? "yes" : "no");
	print(line);
	
	switch(dev.session) {
		case BORT_LAUNCH_SESSION: sprintf(line, "\r\nMCP session: launch;"); break;
		case BORT_SHUTDOWN_SESSION: sprintf(line, "\r\nMCP session: shutdown;"); break;
		case BORT_SESSION: sprintf(line, "\r\nMCP session: bort;"); break;
		case USER_SESSION: sprintf(line, "\r\nMCP session: user;"); break;
		default:	sprintf(line, "\r\nMCP session: none;"); break;
	}
	print(line);
	
	unsigned long hours = (counter * 15 / 10) / 3600;
	unsigned int minuts = (counter * 15 / 10) % 3600 / 60;
	unsigned int seconds = (counter * 15 / 10) % 60;
	
	counter++;
	sprintf(line, "\r\nController during operation : %ldh %02dm %02ds.", 
			(unsigned long) hours, 
			(unsigned int) minuts, 
			(unsigned int) seconds);
	print(line);
}

void print(char *line) {
	for(uint8_t i = 0; i < strlen(line); i++) usart_putchar(&USARTD0, line[i]);
}

inline uint16_t get_battery_temp() {
	return ((battery.map.temperature >> 8) | (battery.map.temperature << 8)) >> 8;
}

inline uint16_t get_battery_current() {
	return ((battery.map.current >> 8) | (battery.map.current << 8)) * 156.3;
}

inline uint16_t get_battery_voltage() {
	return (((battery.map.voltage >> 8) | (battery.map.voltage << 8)) >> 5 ) * 4.88 * 4;
}