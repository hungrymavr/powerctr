/*
 * battery.c
 *
 * Created: 24.09.2014 16:12:55
 *  Author: ale
 */ 

#include <asf.h>
#include <string.h>
#include "battery.h"
#include "i2c.h"

void battery_init() {
	battery.query = &battery_query;
	battery.charge = &battery_charge;
	battery.nocharge = &battery_nocharge;
	battery.charge_possible = &battery_is_charge_possible;
	battery.condition = &battery_is_condition;
	
	battery.get_current = &battery_get_current;
	battery.get_temp = &battery_get_temperature;
	battery.get_volt = &battery_get_voltage;
	
	battery.get_addr_map = &battery_get_map_address;
	
	battery.query();
}

bool battery_query() {
	battery.connect = false;
		
	twi_package_t pkg_wr = {
		.addr_length = 0,
		.chip        = DS2782_ADDR,
		.buffer      = (void *) &ds_pkg,
		.length      = DS2782_PKG_SIZE,
		.no_wait     = false
	};
		
	if(twi_master_write(&TWI_MASTER, &pkg_wr) != STATUS_OK) return false;
		
	memset(&battery.map, 0x00, sizeof(battery.map));

	twi_package_t pkg_rd = {
		.addr		 = {0x00},
		.addr_length = 1,
		.chip        = DS2782_ADDR,
		.buffer      = (void *) &battery.map,
		.length      = 256,
		.no_wait     = false
	};
	if(twi_master_read(&TWI_MASTER, &pkg_rd) != 0) return false;
		
	if(battery.map.status == 0x00)  return false;
				
	battery.connect = true;
	return true;
}

bool battery_charge() {
	twi_package_t pkg;
	
	if(!battery.charge_possible()) return false;
	
	battery.connect = true;
	
	pkg = max8731_get_pkg(MAX8731_SET_CURRENT, MAX8731_CHARGE_CURRENT);
	if(twi_master_write(&TWI_MASTER, &pkg) != STATUS_OK) battery.connect = false;
		
	pkg = max8731_get_pkg(MAX8731_SET_VOLT, MAX8731_CHARGE_VOLT);
	if(twi_master_write(&TWI_MASTER, &pkg) != STATUS_OK) battery.connect = false;
		
	pkg = max8731_get_pkg(MAX8731_SET_INCURRENT, MAX8731_INCURRENT);
	if(twi_master_write(&TWI_MASTER, &pkg) != STATUS_OK) battery.connect = false;
	
	if(!battery.connect) return false;
	
	battery.charging = true;	
	return true;
}

bool battery_nocharge() {
	twi_package_t pkg;
	
	pkg = max8731_get_pkg(MAX8731_SET_CURRENT, 0x00);
	if(twi_master_write(&TWI_MASTER, &pkg) != STATUS_OK) return false;
	
	pkg = max8731_get_pkg(MAX8731_SET_VOLT, 0x00);
	if(twi_master_write(&TWI_MASTER, &pkg) != STATUS_OK) return false;
		
	battery.charging = false;
	return true;
}

bool battery_is_charge_possible() {
	/* temperature */
	int temp = battery.get_temp();
	if((temp > 40) || (temp < -10)) return false;
	return true;
}

bool battery_is_condition() {
	if(battery.get_volt() < ALARM_VOLTAGE ) return false;
	return true;
}

inline uint16_t battery_get_temperature() {
	return ((battery.map.temperature >> 8) | (battery.map.temperature << 8)) / 32 * 125 / 100 / 10; // round to decimal
}

inline uint16_t battery_get_current() {
	return ((battery.map.current >> 8) | (battery.map.current << 8));// * 156.3;
}

inline uint32_t battery_get_voltage() {
//	return (((battery.map.voltage & (uint16_t)0xff00U >> 8) | 
//			(battery.map.voltage & (uint16_t)0x00ffU << 8)) >> 5 ) * (uint32_t) 4880 ;
	return (((battery.map.voltage >> 8) | (battery.map.voltage << 8)) >> 5 ) * 4.88 * 4;
}

inline register8_t * battery_get_map_address(register8_t addr) {
	return ((register8_t *) &battery.map) + addr;
}