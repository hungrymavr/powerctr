/*
 * i2c.h
 *
 * Created: 27.08.2014 12:12:06
 *  Author: ale
 */ 


#ifndef I2C_H_
#define I2C_H_

#define TWI_MASTER			TWIE
#define TWI_SLAVE			TWIC	
#define TWI_SPEED			50000


#define DS2782_ADDR			0x34
#define DS2782_PKG_SIZE		1
#define HOST_ADDR			0x34
#define SELF_ADDR			0x20

#define MAX8781_ADDRESS		0x12 >> 1
#define MAX8781_PKG_SIZE	3

#define MCP_SCALE			2
#define TIME_MCP_ON			0
#define TIME_MCP_OFF		3
#define TIME_MCP_OFF_FORCE	0


typedef enum {
	chargemode		= 0x12,
	chargecurrent	= 0x14,
	chargevoltage	= 0x15,
	alarmwarning	= 0x16,
	inputcurrent	= 0x3f,
	chargerspecinfo	= 0x11,
	chargerstatus	= 0x13,
} max8731_command_code;

#define MAX8731_CHARGE_INFO		0x11
#define MAX8731_SET_CHARGE_MODE	0x12
#define MAX8731_CHARGE_STATUS	0x13
#define MAX8731_SET_CURRENT		0x14
#define MAX8731_SET_VOLT		0x15
#define MAX8731_SET_ALARM		0x16
#define MAX8731_SET_INCURRENT	0x3f

#define MAX8731_CHARGE_VOLT		16800
#define MAX8731_CHARGE_CURRENT	0x800
//#define MAX8731_INCURRENT		1664
#define MAX8731_INCURRENT		0x600

typedef struct __attribute__((packed)) {
	uint8_t command;
	uint16_t value;
} max8731_pkg_t;

typedef struct __attribute__((packed)) {
	uint8_t read_address;
} ds2782_pkg_t;

TWI_Slave_t		i2c_slave;
max8731_pkg_t	max_pkg;
ds2782_pkg_t	ds_pkg;

void i2c_init(void);

void i2c_m_query(void);

twi_package_t max8731_get_pkg(uint8_t command, uint16_t value);
bool i2c_m_query_ds2782(void);

void i2c_s_process(void);
void twi_master_c_callback(void);
void twi_master_callback(void);
#endif /* I2C_H_ */