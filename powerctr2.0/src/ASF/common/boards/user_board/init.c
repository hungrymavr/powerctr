/**
 * \file
 *
 * \brief User board initialization template
 *
 */
#define F_CPU 32000000UL

#define K_FREQ 16 
#define K_100MS 3200


#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>

#include "i2c.h"
#include "console.h"
#include "conf_usart_serial.h"
#include "utils/mcp.h"
#include "utils/buttons.h"
#include "battery.h"

/* FUSE sections for production files */
const uint8_t fusedata[] __attribute__ ((section (".fuse"))) =
{0xFF, 0x89, 0xFD, 0xFF, 0xFF}; // actual values
/* LOCKBITS section for production file */
const uint8_t lockbits[] __attribute__ ((section (".lockbits"))) =
{0xFF};

void device_init(void);
void port_init(void);
void timer_init(void);
void uart_init(void);

void board_init(void) {
	irq_initialize_vectors();
	cpu_irq_enable();	
	sysclk_init();
	port_init();
	pmic_init();
	sleepmgr_init();
	i2c_init();
	uart_init();	
	timer_init();
	battery_init();

	device_init();
}

void port_init() {
	/* PORTA. there are device state pins. */
	PORTA.DIR = 0x42; // pins 0,2,3,6 are not enable
	PORTA.INT0MASK = 0x20;
	PORTA.INT1MASK = 0x10;
	PORTA.INTCTRL = PORT_INT0LVL_MED_gc | PORT_INT1LVL_MED_gc;
	PORTA.PIN4CTRL = PORT_ISC_BOTHEDGES_gc;
	PORTA.PIN5CTRL = PORT_ISC_BOTHEDGES_gc;
	
	/* PORTF. there is chute state pin. */
	PORTF.DIR = 0x00;
	PORTF.INTCTRL = PORT_INT0LVL_MED_gc;
	PORTF.INT0MASK = 0x01;
	PORTF.PIN0CTRL = PORT_ISC_FALLING_gc;
	
	/* PORTB. there is on/off button pin. */
	PORTB.DIR &= ~0x01;
	PORTB.INTCTRL = PORT_INT0LVL_HI_gc;
	PORTB.INT0MASK = 0x01;
	PORTB.PIN0CTRL = PORT_ISC_BOTHEDGES_gc;
	
	PORTC.DIR &= ~0x03;
	PORTC.OUT &= ~0x03;
	
	/* PORTD. there is powergood pin. */
	//PORTD.DIR &= ~0x80;
	//PORTD.INTCTRL = PORT_INT0LVL_MED_gc;
	//PORTD.INT0MASK = 0x80;
	//PORTD.PIN7CTRL = PORT_ISC_BOTHEDGES_gc;
	
}

void timer_init() {
	/* Timer TCE0 is used by twi line */
	sysclk_enable_peripheral_clock(&PORTE);
	sysclk_enable_peripheral_clock(&TCE0);
	tc_enable(&TCE0);
	tc_set_overflow_interrupt_callback(&TCE0, twi_master_c_callback);
	tc_set_wgm(&TCE0, TC_WG_NORMAL);
	tc_write_period(&TCE0, K_100MS * 10); // must be 1 second
	tc_set_overflow_interrupt_level(&TCE0, TC_INT_LVL_LO);
	tc_write_clock_source(&TCE0, TC_CLKSEL_DIV1024_gc);
	tc_disable(&TCE0);
	
	
	/* Timer TCC0 used for mcp timeout */
	sysclk_enable_peripheral_clock(&TCC0);
	tc_enable(&TCC0);
	tc_set_overflow_interrupt_callback(&TCC0, mcp_timeout_callback);
	tc_set_wgm(&TCC0, TC_WG_NORMAL);
	tc_write_period(&TCC0, K_100MS * 10); // must be 1 second
	tc_set_overflow_interrupt_level(&TCC0, TC_INT_LVL_LO);
	tc_write_clock_source(&TCC0, TC_CLKSEL_DIV1024_gc);
	tc_disable(&TCC0);

	
	/* Timer TCD0 is used by console */
	tc_enable(&TCD0);
	tc_set_wgm(&TCD0, TC_WG_NORMAL);
	tc_write_period(&TCD0, K_100MS * 15); // set 1.5 sec 
	tc_write_clock_source(&TCD0, TC_CLKSEL_DIV1024_gc);
	tc_set_overflow_interrupt_level(&TCD0, TC_INT_LVL_LO);
	tc_set_overflow_interrupt_callback(&TCD0, console_send);
	
	/* Timer TCD1 is used for power button */
	tc_enable(&TCD1);
	tc_set_wgm(&TCD1, TC_WG_NORMAL);
	tc_write_clock_source(&TCD1, TC_CLKSEL_DIV1024_gc);
	tc_set_overflow_interrupt_level(&TCD1, TC_INT_LVL_HI);
	tc_set_overflow_interrupt_callback(&TCD1, powerbutton_callback);
	tc_disable(&TCD1);
	
	/* Timer TCF0 is used for power timeout */
	tc_enable(&TCF0);
	tc_set_wgm(&TCF0, TC_WG_NORMAL);
	tc_write_period(&TCF0, K_100MS * 10); // set 1 sec
	tc_write_clock_source(&TCF0, TC_CLKSEL_DIV1024_gc);
	tc_set_overflow_interrupt_level(&TCF0, TC_INT_LVL_LO);
	tc_set_overflow_interrupt_callback(&TCF0, mcp_power_timeout_callback);
	tc_disable(&TCF0);
	
	/* Chute antibounce timer */	
	tc_enable(&TCE1);
	tc_set_wgm(&TCE1, TC_WG_NORMAL);
	tc_write_period(&TCE1, K_100MS); // 100 msec
	tc_write_clock_source(&TCE1, TC_CLKSEL_DIV1024_gc);
	tc_set_overflow_interrupt_level(&TCE1, TC_INT_LVL_HI);
	tc_set_overflow_interrupt_callback(&TCE1, antibounce_callback);
	tc_disable(&TCE1);
	
	/* Watchdog */
	wdt_set_timeout_period(WDT_TIMEOUT_PERIOD_2KCLK);
	wdt_enable();
	
}

void uart_init() {
	sysclk_enable_peripheral_clock(&PORTD);
	PORTD.DIRSET = PIN3_bm;
	PORTD.DIRCLR = PIN2_bm;

	static usart_rs232_options_t USART_SERIAL_OPTIONS = {
		.baudrate = USART_SERIAL_BAUDRATE,
		.charlength = USART_SERIAL_CHAR_LENGTH,
		.paritytype = USART_SERIAL_PARITY,
		.stopbits = USART_SERIAL_STOP_BIT
	};	
//	sysclk_enable_module(&PORTD, USART_PR_USART);
	usart_init_rs232(&USARTD0, &USART_SERIAL_OPTIONS);
	
	usart_tx_enable(&USARTD0);
}

void device_init() {
	_delay_ms(50);
	dev.button = BUTTON_UNLOCK;
	dev.session = NONE_SESSION;
	

	if(is_bort() && (!is_mcp_enable())) { // bort enable and mcp not
		mcp_on();
	}
	
	if(is_mcp_enable()) dev.session = BORT_SESSION;
	
}