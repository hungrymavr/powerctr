/*
 * buttons.c
 *
 * Created: 25.09.2014 12:36:02
 *  Author: ale
 */ 

#define K_FREQ 16

#include <asf.h>
#include "utils/buttons.h"
#include "utils/mcp.h"

void button_power_on(uint16_t time) {
	dev.button = BUTTON_LOCK;
	PORTA.OUT = 0x02;
	if(time == 0) return;
	
	tc_enable(&TCD1);
	tc_write_period(&TCD1, time);
	tc_reset(&TCD1);
	tc_restart(&TCD1);
	tc_enable(&TCD1);
}

void powerbutton_callback(void) {
	dev.button = BUTTON_UNLOCK;
	(PORTB.IN & 0x01) ? (PORTA.OUT &= ~0x02) : (PORTA.OUT |= 0x02);
	tc_disable(&TCD1);
}

void antibounce_callback(void) {
	tc_disable(&TCE1);
	
	if(dev.antibounce_object == CHUTE) {
		if((PORTF.IN & 0x01) == 0) 
			mcp_on(); 
	} else 
		(PORTB.IN & 0x01) ? (PORTA.OUT &= ~0x02) : (PORTA.OUT |= 0x02);
}
