/*
 * mcp.h
 *
 * Created: 25.09.2014 12:38:44
 *  Author: ale
 */ 


#ifndef MCP_H_
#define MCP_H_

typedef enum {
	USER_SESSION,
	BORT_SESSION,
	BORT_LAUNCH_SESSION,
	BORT_SHUTDOWN_SESSION,
	NONE_SESSION,
} _mcp_session_type;

typedef enum {
	BUTTON_LOCK,
	BUTTON_UNLOCK,
} _button_processing;

typedef enum {
	ONOFF_BUTTON,
	CHUTE,
} _antibounce_system_object;

typedef struct {
	_antibounce_system_object antibounce_object;
	_button_processing button;
	
	uint16_t time;			// timer for BORT_SHUTDOWN_SESSION			
	_mcp_session_type session;
	
	bool twi_master_processing;
} device_t;

device_t dev;

void twi_master_run(void);
void no_twi_master_run(void);

void mcp_on(void);
void mcp_off(void);
void mcp_timeout_callback(void);
void mcp_power_timeout_callback(void);

bool is_mcp_enable(void);
bool is_charging(void);
bool is_powergood(void);
bool is_bort(void);


#endif /* MCP_H_ */