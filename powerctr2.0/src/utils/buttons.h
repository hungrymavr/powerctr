/*
 * buttons.h
 *
 * Created: 25.09.2014 12:35:49
 *  Author: ale
 */ 


#ifndef BUTTONS_H_
#define BUTTONS_H_

void button_power_on(uint16_t time);
void powerbutton_callback(void);
void antibounce_callback(void);

#endif /* BUTTONS_H_ */